CC=gcc
CFLAGS=-c

run: main.o
	$(CC) -o run  main.o

main.o: main.c
	$(CC) $(CFLAGS) main.c

.PHONY: clean
clean:
	rm *o run
